import {Navigation} from 'react-native-navigation';
import {registerScreen} from './app/screens';

registerScreen();

Navigation.startTabBasedApp({
    tabs: [
        {
            label: 'One',
            screen: 'example.FirstTabScreen',
            title: 'Screen One',
            icon: require('./img/one.png'),
        },
        {
            label: 'Two',
            screen: 'example.SecondTabScreen',
            title: 'Screen Two',
            icon: require('./img/one.png'),
        }
    ]
});