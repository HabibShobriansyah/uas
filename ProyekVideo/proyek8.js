import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

export default class App extends React.Component{

    render(){
        const myName ="Habib Shobriansyah";
        return(
        <View style={styles.container}>
            <View style={styles.header}>
            <Text>Header</Text>
            </View>
            <View style={styles.footer}>
            <Text>Footer</Text>
            </View>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 0.2,
        backgroundColor: "green",
        justifyContent: 'center'
    },
    footer: {
        flex: 2,
        backgroundColor: "blue"
    }, 
    footerText: {
        alignSelf: 'center',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20
    }
});